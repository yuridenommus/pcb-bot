{
  description = "Um bot para gerenciar os recrutamentos do PCB";

  inputs = {
    cargo2nix.url = "github:cargo2nix/cargo2nix/release-0.11.0";
    flake-utils.follows = "cargo2nix/flake-utils";
    nixpkgs.follows = "cargo2nix/nixpkgs";
  };

  outputs = { self, nixpkgs, flake-utils, cargo2nix }:
  flake-utils.lib.eachDefaultSystem (system:
  let
    cargoToml = builtins.fromTOML (builtins.readFile ./Cargo.toml);
    toolchainToml = builtins.fromTOML (builtins.readFile ./rust-toolchain.toml);
    name = cargoToml.package.name;
    pkgs = import nixpkgs {
      inherit system;
      overlays = [
        (final: prev: let
          pkg = final.rustPkgs.workspace."${name}" {};
        in {
          "${name}" = pkg.bin;
          "${name}-deps" = final.stdenv.mkDerivation {
            name = "${name}-deps";
            installPhase = ''
              mkdir -p $out
              touch $out/${name}-deps
            '';
            src = ./Cargo.nix;
            unpackPhase = "true";
            propagatedBuildInputs = pkg.buildInputs
                                    ++ pkg.propagatedBuildInputs
                                    ++ pkg.nativeBuildInputs
                                    ++ pkg.propagatedNativeBuildInputs
                                    ++ [
                                      final.diesel-cli
                                    ];
          };
          "${name}-docker" = final.dockerTools.buildImage {
            name = "registry.gitlab.com/yuridenommus/pcb-bot";
            tag = "latest";
            contents = [
              final."${name}"
              final.diesel-cli
            ];
          };
        })
        (cargo2nix.overlay)
        (final: prev: {
          rustPkgs = final.rustBuilder.makePackageSet {
            rustVersion = toolchainToml.toolchain.channel;
            rustProfile = toolchainToml.toolchain.profile;
            extraRustComponents = toolchainToml.toolchain.components;
            packageFun = import ./Cargo.nix;
            packageOverrides = rpkgs: rpkgs.rustBuilder.overrides.all ++ [
              (rpkgs.rustBuilder.rustLib.makeOverride {
                name = "libsqlite3-sys";
                overrideAttrs = old: {
                  propagatedBuildInputs = (old.propagatedBuildInputs or [ ]) ++ [
                    rpkgs.sqlite
                  ];
                };
              })
            ];
          };
          diesel-cli = (builtins.elemAt
            (builtins.attrValues final.rustPkgs."registry+https\://github.com/rust-lang/crates.io-index".diesel_cli)
            0) {};
          new-rust-analyzer = final.rustPkgs.rustPackages.pkgs.rust-analyzer;
        })
      ];
    };
    app = {
      type = "app";
      program = "${pkgs."${name}"}/bin/${name}";
    };
  in
  {
    inherit pkgs;
    packages = {
      "${name}" = pkgs."${name}";
      default = pkgs."${name}";
      "${name}-deps" = pkgs."${name}-deps";
      diesel-cli = pkgs.diesel-cli;
    } // (if pkgs.stdenv.isLinux then {
      "${name}-docker" = pkgs."${name}-docker";
    } else {});

    apps = {
      "${name}" = app;
      default = app;
    };

    devShell = pkgs.rustPkgs.workspaceShell {
      shellHook = ''
        export DATABASE_URL=database.sqlite
      '';
      buildInputs = with pkgs; [
        nixpkgs-fmt
        new-rust-analyzer
        diesel-cli
        pandoc
      ];
    };
  });
}

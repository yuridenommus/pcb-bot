PRAGMA foreign_keys = ON;

CREATE TABLE federations (
  id TEXT PRIMARY KEY NOT NULL,
  title TEXT NOT NULL
);

CREATE TABLE groups (
  id INTEGER PRIMARY KEY NOT NULL,
  federation_id TEXT NOT NULL,
  parent_group INTEGER,
  FOREIGN KEY (federation_id) REFERENCES federations(id),
  FOREIGN KEY (parent_group) REFERENCES groups(id)
);

CREATE TABLE recruiters (
  id INTEGER PRIMARY KEY NOT NULL,
  telegram_id BIGINT NOT NULL,
  telegram_username TEXT,
  first_name TEXT NOT NULL,
  last_name TEXT
);
CREATE UNIQUE INDEX recruiter_user_id ON recruiters(telegram_id);
CREATE UNIQUE INDEX recruiter_username ON recruiters(telegram_username);
CREATE INDEX recruiter_name ON recruiters(first_name);

CREATE TABLE recruits (
  id TEXT PRIMARY KEY NOT NULL,
  name TEXT NOT NULL,
  email TEXT,
  phone TEXT,
  telegram_id BIGINT,
  telegram_username TEXT,
  recruiter_1 INTEGER,
  recruiter_2 INTEGER,
  responsible_group INTEGER NOT NULL,
  FOREIGN KEY (recruiter_1) REFERENCES recruiters(id),
  FOREIGN KEY (recruiter_2) REFERENCES recruiters(id),
  FOREIGN KEY (responsible_group) REFERENCES groups(id)
);
CREATE INDEX recruit_name ON recruits(name);
CREATE UNIQUE INDEX recruit_username ON recruits(telegram_username);

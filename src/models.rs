use crate::{
    db,
    schema::{federations, groups},
};
use chrono::Utc;
use diesel::{Identifiable, Insertable, Queryable};
use tokio_diesel::AsyncRunQueryDsl;
use uuid::Uuid;

#[derive(Queryable, Identifiable)]
#[table_name = "federations"]
pub struct Federation {
    pub id: String,
    pub title: String,
}

impl Federation {
    pub async fn delete(
        conn: &db::ConnectionPool,
        fid: &str,
    ) -> Result<(), Box<dyn std::error::Error>> {
        use crate::schema::federations::dsl::*;
        use diesel::prelude::*;
        diesel::delete(federations.filter(id.eq(fid)))
            .execute_async(conn)
            .await?;
        Ok(())
    }
}

#[derive(Insertable)]
#[table_name = "federations"]
pub struct InsertFederation<'a> {
    pub id: &'a str,
    pub title: &'a str,
}

impl<'a> InsertFederation<'a> {
    pub async fn insert(
        conn: &db::ConnectionPool,
        new_title: &'a str,
    ) -> Result<Federation, Box<dyn std::error::Error>> {
        let bytes: Vec<u8> = [new_title.as_bytes(), Utc::now().to_string().as_bytes()].concat();
        let uuid = Uuid::from_slice(&bytes)?.to_string();

        {
            use crate::schema::federations::dsl::*;

            diesel::insert_into(federations)
                .values(&InsertFederation {
                    id: &uuid,
                    title: new_title,
                })
                .execute_async(conn)
                .await?;
        }
        Ok(Federation {
            id: uuid,
            title: new_title.to_string(),
        })
    }
}

#[derive(Queryable, Identifiable)]
#[table_name = "groups"]
pub struct Group {
    pub id: i32,
    pub federation: Federation,
    pub parent_group: i32,
}

#[derive(Insertable)]
#[table_name = "groups"]
pub struct InsertGroup<'a> {
    pub id: i32,
    pub federation_id: &'a str,
    parent_group: i32,
}

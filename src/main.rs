use actix_web::{web, App, HttpServer};
use std::{collections::HashMap, env, error::Error};

#[macro_use]
extern crate diesel;

mod adapters;
mod commands;
mod db;
mod models;
mod routes;
#[rustfmt::skip]
mod schema;

#[derive(Clone)]
pub struct AppState {
    pub db_conn: db::ConnectionPool,
    pub telegram_api: telegram_bot::Api,
}

type AppError = Box<dyn Error>;

#[tokio::main]
async fn main() -> Result<(), AppError> {
    dotenv::dotenv()?;
    let token = env::var("PCB_BOT_TOKEN").or_else(|_| dotenv::var("PCB_BOT_TOKEN"))?;

    let db_url = env::var("DATABASE_URL").or_else(|_| dotenv::var("DATABASE_URL"))?;

    let app_state = AppState {
        db_conn: db::pool(&db_url)?,
        telegram_api: telegram_bot::Api::new(&token),
    };

    let virtual_host = env::var("VIRTUAL_HOST")?;

    let route = format!("/{}", token.as_str());

    let telegram_url = "https://api.telegram.org/bot";
    let set_webhook = format!("{}{}/setWebhook", telegram_url, token);
    let delete_webhook = format!("{}{}/deleteWebhook", telegram_url, token);
    let url = format!("https://{}{}", virtual_host, route);
    let request_body = HashMap::from([("url", url)]);

    let client = reqwest::Client::new();
    let set_response = client
        .post(set_webhook.as_str())
        .json(&request_body)
        .send()
        .await?;
    println!("Telegram's response {}", set_response.text().await?);

    HttpServer::new(move || {
        let data = web::Data::new(app_state.clone());
        App::new()
            .app_data(data)
            .service(web::scope(route.as_str()).service(routes::index))
    })
    .bind(("0.0.0.0", 8000))?
    .run()
    .await
    .map_err(|x| Box::new(x) as Box<dyn Error>)?;

    let delete_response = client.post(delete_webhook.as_str()).send().await?;
    println!("Telegram's response {}", delete_response.text().await?);

    Ok(())
}

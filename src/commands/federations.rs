use telegram_bot::{Message, SendMessage};

use crate::{models::InsertFederation, AppState};

pub async fn new(
    state: &AppState,
    message: &Message,
    arguments: &str,
) -> Result<(), Box<dyn std::error::Error>> {
    let federation = InsertFederation::insert(&state.db_conn, arguments).await?;
    state
        .telegram_api
        .send(SendMessage::new(message.chat.id(), federation.id).reply_to(message))
        .await?;
    Ok(())
}

pub fn delete(_message: &Message, _arguments: &str) -> Result<(), Box<dyn std::error::Error>> {
    Ok(())
}

pub fn merge(_message: &Message, _arguments: &str) -> Result<(), Box<dyn std::error::Error>> {
    Ok(())
}

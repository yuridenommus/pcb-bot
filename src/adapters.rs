use regex::Regex;

pub fn parse_command(original: &str) -> Option<(String, String)> {
    let re = Regex::new(r"^/([a-zA-Z]+)(@[a-zA-Z]+)? (.+)$").ok()?;
    let caps = re.captures(original)?;
    let command = caps.get(1)?;
    let args = caps.get(3)?;
    return Some((command.as_str().into(), args.as_str().into()));
}

use crate::{adapters, commands, AppState};
use actix_web::{post, web, HttpResponse, Responder};
use telegram_bot::{MessageKind, Update, UpdateKind};

#[post("/", name = "recrutar")]
async fn index(state: web::Data<AppState>, update: web::Json<Update>) -> impl Responder {
    let update = update.into_inner();
    let state = state.into_inner();
    println!("Made it here");
    println!("Update id: {}", update.id);
    if let UpdateKind::Message(ref message) = update.kind {
        if let MessageKind::Text {
            ref data,
            ref entities,
        } = message.kind
        {
            println!("{:?} {:?}", data, entities);
            let (command, arguments) = adapters::parse_command(data).unwrap_or_default();

            let result = match command.as_str() {
                "/recrutar" => Ok(()),
                "/mudar" => Ok(()),
                "/assumir" => Ok(()),
                "/cancelar" => Ok(()),
                "/fnova" => commands::federations::new(&state, message, &arguments).await,
                "/fexcluir" => commands::federations::delete(message, &arguments),
                "/fmesclar" => commands::federations::merge(message, &arguments),
                "/glista" => Ok(()),
                "/gpai" => Ok(()),
                "/gfilho" => Ok(()),
                "/gentrar" => Ok(()),
                "/gsair" => Ok(()),
                _ => {
                    return HttpResponse::MethodNotAllowed();
                }
            };
            return result
                .map(|_| HttpResponse::Ok())
                .unwrap_or_else(|_| HttpResponse::BadRequest());
        }
    }
    HttpResponse::MethodNotAllowed()
}

table! {
    federations (id) {
        id -> Text,
        title -> Text,
    }
}

table! {
    groups (id) {
        id -> Integer,
        federation_id -> Text,
        parent_group -> Nullable<Integer>,
    }
}

table! {
    recruiters (id) {
        id -> Integer,
        telegram_id -> BigInt,
        telegram_username -> Nullable<Text>,
        first_name -> Text,
        last_name -> Nullable<Text>,
    }
}

table! {
    recruits (id) {
        id -> Text,
        name -> Text,
        email -> Nullable<Text>,
        phone -> Nullable<Text>,
        telegram_id -> Nullable<BigInt>,
        telegram_username -> Nullable<Text>,
        recruiter_1 -> Nullable<Integer>,
        recruiter_2 -> Nullable<Integer>,
        responsible_group -> Integer,
    }
}

joinable!(groups -> federations (federation_id));
joinable!(recruits -> groups (responsible_group));

allow_tables_to_appear_in_same_query!(
    federations,
    groups,
    recruiters,
    recruits,
);

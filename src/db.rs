use diesel::{r2d2::ConnectionManager, SqliteConnection};
use r2d2::{Error, Pool};

pub type ConnectionPool = Pool<ConnectionManager<SqliteConnection>>;

pub fn pool(db_url: &str) -> Result<ConnectionPool, Error> {
    let manager = ConnectionManager::new(db_url);
    Pool::new(manager)
}
